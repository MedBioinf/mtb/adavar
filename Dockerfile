FROM python:3.12.7-bookworm

# Updates
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y python3-pip python3-dev iputils-ping
RUN python3 -m pip install --upgrade pip

# Create virtual environment
RUN python3 -m venv /opt/venv
RUN /opt/venv/bin/python3 -m pip install --upgrade pip

WORKDIR /app
COPY requirements.txt /app/

RUN pip install --no-cache-dir -r requirements.txt
RUN /opt/venv/bin/pip install -r requirements.txt
RUN chmod -R 777 /opt/venv/lib

RUN pip install onkopus

COPY . /app

EXPOSE 11108

#ENV DASH_DEBUG_MODE True

CMD ["python", "flask_app.py"]
