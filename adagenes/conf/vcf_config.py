
reserved_info_fields = [
    "AA", "AC", "AD", "ADF", "ADR", "AF", "AN", "BQ", "CIGAR", "DB", "DP", "END", "H2", "H3", "MQ", "MQ0", "NS", "SB",
    "SOMATIC", "VALIDATED", "1000G"
]

additional_info_fields = [
    "AO", "FAO", "FDP", "FDVR", "FR", "FRO", "FSAF", "FSAR", "FSRF", "FSRR", "FWDB", "FXX", "HRUN", "HS_ONLY", "LEN",
    "MLLD", "OALT", "OID", "OMAPALT", "OPOS", "OREF", "PB", "PBP", "PPD", "QD", "RBI", "REFB", "REVB", "RO", "SAF",
    "SAR", "SPD", "SRF", "SRR", "SSEN", "SSEP", "SSSB", "STB", "STBP", "TYPE", "VARB"
]

base_info_line = "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO"
genome_version_line = "##reference={}"


