

The data files ("UCSC Chain Files") in this directory are property of The
Regents of the University of California, and made available free for
non-commercial use by Independent Researchers and Nonprofit Organizations. Any
other use of UCSC Chain Files requires a commercial license, for which users
should contact genomebrowser@ucsc.edu. As used herein, "Independent
Researcher" means an individual who is autonomous with respect to the
research activities for which he or she uses the UCSC Chain Files (note: such
use does not extend to any use of the UCSC Chain Files at the direction and/or
for the benefit of a for-profit organization); and "Nonprofit
Organization" means a university or other institution of higher education,
or a not-for-profit organization officially recognized or qualified under the
laws of the country in which it is organized or located, or any nonprofit
scientific or educational organization qualified under a federal, state or local
jurisdiction's nonprofit organization statute. Downloading or using UCSC Chain
Files indicates your acceptance of the End User License Agreement (EULA) posted
at "https://genome.ucsc.edu/license/EULA.pdf"; redistribution of UCSC Chain
Files by you must include this README file with these license restrictions, in
the same archive or directory with the chain files.

This directory contains the data files required as input to the
liftOver utility. This tool -- which requires a Linux platform --
allows the mass conversion of coordinates from one assembly to
another. The executable file for the utility can be downloaded from
http://hgdownload.soe.ucsc.edu/admin/exe/liftOver.gz .

The file names reflect the assembly conversion data contained within
in the format <db1>To<Db2>.over.chain.gz. For example, a file named
hg15ToHg16.over.chain.gz file contains the liftOver data needed to
convert hg15 (Human Build 33) coordinates to hg16 (Human Build 34).
If no file is available for the assembly in which you're interested,
please send a request to the genome mailing list
(genome@soe.ucsc.edu) and we will attempt to provide you with one.

To download a large file or multiple files from this directory,
we recommend that you use ftp rather than downloading the files via our
website. To do so, ftp to hgdownload.soe.ucsc.edu (user: anonymous),
then cd to goldenPath/hg19/liftOver.  To download multiple files,
use the "mget" command:

    mget <filename1> <filename2> ...
    - or -
    mget -a (to download all the files in the directory)

-------------------------------------------------------
Please refer to the credits page
(http://genome.ucsc.edu/goldenPath/credits.html) for guidelines and
restrictions regarding data use for these assemblies.
-------------------------------------------------------
Alternate methods to ftp access.

Using an rsync command to download the entire directory:
    rsync -avzP rsync://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/ .
For a single file, e.g. hg19ToHg18.over.chain.gz
    rsync -avzP
        rsync://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg18.over.chain.gz .
    (Hg18 is merely an example here, not necessarily existing.)

Or with wget, all files:
    wget --timestamping
        'ftp://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/*'
With wget, a single file:
    wget --timestamping
        'ftp://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg18.over.chain.gz'
        -O hg19ToHg18.over.chain.gz

To uncompress the *.chain.gz files:
    gunzip <file>.chain.gz
The liftOver utility can read the files in their .gz format,
it is not necessary to uncompress them to use with the liftOver command.

      Name                                Last modified      Size  Description      Parent Directory                                         -
      hg19ToAilMel1.over.chain.gz         2010-02-04 16:31   78M
      hg19ToAllMis1.over.chain.gz         2013-06-28 14:03   17M
      hg19ToAnoCar1.over.chain.gz         2009-05-30 22:56  8.0M
      hg19ToAnoCar2.over.chain.gz         2011-04-19 13:16  7.8M
      hg19ToAplCal1.over.chain.gz         2009-06-09 12:31  1.9M
      hg19ToBalAcu1.over.chain.gz         2016-08-19 03:03   80M
      hg19ToBosTau4.over.chain.gz         2009-06-04 12:27   81M
      hg19ToBosTau6.over.chain.gz         2011-05-16 20:08   82M
      hg19ToBosTau7.over.chain.gz         2012-01-23 14:42   81M
      hg19ToBosTau8.over.chain.gz         2022-10-13 20:19   84M
      hg19ToBosTau9.over.chain.gz         2020-12-07 12:55   83M
      hg19ToCalJac1.over.chain.gz         2009-05-14 12:57   52M
      hg19ToCalJac3.over.chain.gz         2010-02-11 13:46   50M
      hg19ToCanFam2.over.chain.gz         2009-05-14 01:08   88M
      hg19ToCanFam3.over.chain.gz         2012-07-04 02:20   87M
      hg19ToCavPor3.over.chain.gz         2009-06-04 15:20   83M
      hg19ToCerSim1.over.chain.gz         2012-10-19 14:06   82M
      hg19ToChlSab1.over.chain.gz         2013-06-30 11:38   36M
      hg19ToChoHof1.over.chain.gz         2009-06-05 14:20   67M
      hg19ToCriGri1.over.chain.gz         2013-09-01 02:51   71M
      hg19ToDanRer5.over.chain.gz         2009-05-26 19:49  6.4M
      hg19ToDanRer6.over.chain.gz         2009-07-10 11:40  7.7M
      hg19ToDanRer7.over.chain.gz         2010-12-18 12:42  6.9M
      hg19ToDanRer10.over.chain.gz        2015-09-25 08:30  7.2M
      hg19ToDasNov2.over.chain.gz         2009-05-27 14:12   67M
      hg19ToDasNov3.over.chain.gz         2013-07-09 13:45   83M
      hg19ToDipOrd1.over.chain.gz         2009-05-29 19:38   58M
      hg19ToEchTel1.over.chain.gz         2012-06-29 16:16   54M
      hg19ToEchTel2.over.chain.gz         2013-06-17 22:34   61M
      hg19ToEquCab2.over.chain.gz         2009-06-04 13:02   82M
      hg19ToEriEur1.over.chain.gz         2009-05-30 07:11   46M
      hg19ToEriEur2.over.chain.gz         2013-07-09 08:24   53M
      hg19ToFelCat3.over.chain.gz         2009-06-04 15:32   67M
      hg19ToFelCat4.over.chain.gz         2010-06-08 11:22   75M
      hg19ToFelCat5.over.chain.gz         2014-02-27 13:53   86M
      hg19ToFr2.over.chain.gz             2009-05-20 16:19  3.9M
      hg19ToGCF_002742125.1.over.chain.gz 2022-10-20 02:35   84M
      hg19ToGCF_014441545.1.over.chain.gz 2023-02-25 03:08   90M
      hg19ToGCF_017639785.1.over.chain.gz 2023-03-01 15:13   69M
      hg19ToGCF_019923935.1.over.chain.gz 2024-01-04 17:40   84M
      hg19ToGalGal3.over.chain.gz         2009-05-14 09:12  7.4M
      hg19ToGalGal4.over.chain.gz         2013-06-29 23:12  7.5M
      hg19ToGasAcu1.over.chain.gz         2009-05-14 13:56  4.5M
      hg19ToGeoFor1.over.chain.gz         2012-07-29 14:41  7.2M
      hg19ToGorGor1.over.chain.gz         2009-03-22 02:59   27M
      hg19ToGorGor3.over.chain.gz         2011-10-17 12:44   15M
      hg19ToGorGor5.over.chain.gz         2017-11-08 14:56   18M
      hg19ToGorGor6.over.chain.gz         2021-06-28 11:44   14M
      hg19ToHg15.over.chain.gz            2023-02-17 10:36  295K
      hg19ToHg17.over.chain.gz            2012-11-09 15:45  341K
      hg19ToHg18.over.chain.gz            2009-06-04 15:37  221K
      hg19ToHg38.over.chain.gz            2013-12-31 23:08  222K
      hg19ToLoxAfr3.over.chain.gz         2009-07-22 12:24   79M
      hg19ToMacEug1.over.chain.gz         2009-05-31 04:35   18M
      hg19ToMacFas5.over.chain.gz         2013-06-30 18:50   35M
      hg19ToMelGal1.over.chain.gz         2011-03-28 13:01  5.4M
      hg19ToMicMur1.over.chain.gz         2009-05-22 19:16   71M
      hg19ToMm9.over.chain.gz             2009-05-13 20:56   75M
      hg19ToMm10.over.chain.gz            2012-03-07 18:40   75M
      hg19ToMonDom5.over.chain.gz         2009-05-29 06:34   32M
      hg19ToMyoLuc1.over.chain.gz         2009-06-04 18:38   63M
      hg19ToMyoLuc2.over.chain.gz         2013-08-30 01:44   69M
      hg19ToNomLeu1.over.chain.gz         2011-11-05 01:05   26M
      hg19ToNomLeu3.over.chain.gz         2013-03-22 16:40   26M
      hg19ToOchPri2.over.chain.gz         2009-05-30 00:54   59M
      hg19ToOchPri3.over.chain.gz         2013-06-18 03:01   67M
      hg19ToOrnAna1.over.chain.gz         2009-05-27 01:17   18M
      hg19ToOryCun1.over.chain.gz         2009-05-29 06:43   67M
      hg19ToOryCun2.over.chain.gz         2009-08-12 21:48   78M
      hg19ToOryLat2.over.chain.gz         2009-05-22 18:25  4.4M
      hg19ToOtoGar1.over.chain.gz         2009-05-15 02:08   77M
      hg19ToOviAri1.over.chain.gz         2010-04-16 16:21   57M
      hg19ToOviAri3.over.chain.gz         2013-06-26 14:32   81M
      hg19ToPanPan2.over.chain.gz         2018-05-01 22:20   16M
      hg19ToPanPan3.over.chain.gz         2023-08-30 01:12   12M
      hg19ToPanTro2.over.chain.gz         2009-03-19 23:34   13M
      hg19ToPanTro3.over.chain.gz         2011-02-22 16:33   13M
      hg19ToPanTro4.over.chain.gz         2013-01-25 14:29   13M
      hg19ToPanTro5.over.chain.gz         2017-05-04 17:03   13M
      hg19ToPanTro6.over.chain.gz         2018-12-14 12:15   12M
      hg19ToPapAnu2.over.chain.gz         2013-08-19 23:49   35M
      hg19ToPapHam1.over.chain.gz         2009-05-20 17:56   40M
      hg19ToPetMar1.over.chain.gz         2009-05-14 11:24  3.0M
      hg19ToPetMar2.over.chain.gz         2012-10-17 12:59  2.8M
      hg19ToPonAbe2.over.chain.gz         2009-05-14 03:22   26M
      hg19ToPonAbe3.over.chain.gz         2018-12-14 12:17   23M
      hg19ToProCap1.over.chain.gz         2009-05-26 12:27   64M
      hg19ToPteVam1.over.chain.gz         2009-06-04 09:50   75M
      hg19ToRheMac2.over.chain.gz         2009-05-14 08:53   34M
      hg19ToRheMac3.over.chain.gz         2012-03-15 19:48   36M
      hg19ToRheMac8.over.chain.gz         2017-07-06 13:48   37M
      hg19ToRheMac10.over.chain.gz        2019-07-09 09:57   37M
      hg19ToRn4.over.chain.gz             2009-05-13 20:44   70M
      hg19ToRn5.over.chain.gz             2012-06-27 17:52   67M
      hg19ToRn6.over.chain.gz             2015-06-08 13:42   67M
      hg19ToSaiBol1.over.chain.gz         2013-06-30 17:42   48M
      hg19ToSarHar1.over.chain.gz         2013-07-10 13:00   16M
      hg19ToSorAra1.over.chain.gz         2009-05-29 08:40   46M
      hg19ToSorAra2.over.chain.gz         2013-06-18 16:24   58M
      hg19ToSpeTri1.over.chain.gz         2009-06-04 20:15   69M
      hg19ToSpeTri2.over.chain.gz         2013-08-28 23:27   87M
      hg19ToSusScr2.over.chain.gz         2010-03-27 02:13   71M
      hg19ToSusScr3.over.chain.gz         2013-08-29 00:59   79M
      hg19ToSusScr11.over.chain.gz        2018-04-02 23:15   84M
      hg19ToTaeGut1.over.chain.gz         2009-05-26 13:04  7.2M
      hg19ToTaeGut2.over.chain.gz         2013-06-17 23:33   13M
      hg19ToTarSyr1.over.chain.gz         2009-05-15 16:52   78M
      hg19ToTetNig1.over.chain.gz         2009-05-14 13:41  4.9M
      hg19ToTetNig2.over.chain.gz         2009-08-10 14:17  4.0M
      hg19ToTupBel1.over.chain.gz         2009-06-01 11:22   69M
      hg19ToTurTru1.over.chain.gz         2009-06-04 13:51   77M
      hg19ToVicPac1.over.chain.gz         2009-06-04 22:02   67M
      hg19ToXenTro2.over.chain.gz         2009-05-27 11:15  7.5M
      hg19ToXenTro3.over.chain.gz         2011-09-20 17:11  7.1M
      md5sum.txt                          2024-01-11 16:16  9.1K

