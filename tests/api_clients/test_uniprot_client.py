import unittest, copy
import adagenes as ag

class UniprotIDTestCase(unittest.TestCase):

    def test_uniprotid_client(self):
        data={
            "P31946":{ "uniprot_id":"P31946"  },
            "P62258:A40V": { "uniprot_id":"P62258" },
            "Q04917":{ "uniprot_id":"Q04917" },
            "P61981":{ "uniprot_id":"P61981" },
            "P31947:E65T": { "uniprot_id":"P31947" }
        }

        variant_data = ag.UniprotClient().process_data(data)
        print("Response ",variant_data)
        self.assertEqual(variant_data["P31946"]["gene_name"],"YWHAB","")
        self.assertEqual(variant_data["Q04917"]["gene_name"], "YWHAH", "")


