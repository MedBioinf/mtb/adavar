import unittest, os
import adagenes


class TestVariantTypeFilter(unittest.TestCase):

    def test_type_filter(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        genome_version = 'hg19'

        infile = __location__ + "/../test_files/somaticMutations.ln50.revel.csv"
        bframe = adagenes.read_file(infile, genome_version=genome_version)
        bframe = adagenes.TypeRecognitionClient().process_data(bframe)
        get_types=["indel"]
        bframe.data = adagenes.TypeFilterClient().process_data(bframe.data, get_types=get_types)
        print(bframe.data)


