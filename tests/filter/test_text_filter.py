import unittest, os
import adagenes


class TestTextFilter(unittest.TestCase):

    def test_text_filter(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        genome_version = 'hg19'

        infile = __location__ + "/../test_files/somaticMutations.ln50.revel.csv"
        bframe = adagenes.read_file(infile, genome_version=genome_version)
        print("pre ",bframe)
        filter_arg = ['alt','A','contains']
        bframe.data = adagenes.TextFilter(filter=filter_arg).process_data(bframe.data)
        print(len(bframe.data.keys()))
        print(bframe)
        self.assertEqual(len(bframe.data.keys()),21,"error chromosome filter")


