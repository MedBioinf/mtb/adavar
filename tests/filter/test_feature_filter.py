import unittest, os
import adagenes


class TestFeatureFilter(unittest.TestCase):

    def test_feature_filter_numeric(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../test_files/somaticMutations.ln50.revel.csv"
        bframe = adagenes.read_file(infile)
        mapping = {""}
        filter = ["revel", "0.5", ">"]

        bframe_filtered = adagenes.FeatureFilter().process_data(bframe, filter)
        print(len(bframe.data.keys()))
        print(len(bframe_filtered.data.keys()))

        self.assertEqual(len(bframe.data.keys()),48,"Error biomarker data length")
        self.assertEqual(len(bframe_filtered.data.keys()), 14, "Error biomarker data length")

    def test_feature_filter_defined_values(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../test_files/somaticMutations.ln50.dbsnp.dbnsfp.avf"
        bframe = adagenes.read_file(infile)
        print("dbsnp" , bframe)

        mapping = {""}
        filter = ["dbsnp:freq_total", "0.5", ">"]

        #bframe_filtered = adagenes.FeatureFilter().process_data(bframe, filter)
        #print(len(bframe.data.keys()))
        #print(len(bframe_filtered.data.keys()))

        #self.assertEqual(len(bframe.data.keys()),48,"Error biomarker data length")
        #self.assertEqual(len(bframe_filtered.data.keys()), 14, "Error biomarker data length")

