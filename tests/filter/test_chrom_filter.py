import unittest, os
import adagenes


class TestChromFilter(unittest.TestCase):

    def test_type_filter(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        genome_version = 'hg19'

        infile = __location__ + "/../test_files/somaticMutations.ln50.revel.csv"
        bframe = adagenes.read_file(infile, genome_version=genome_version)
        print("pre ",bframe)
        bframe.data = adagenes.ChromosomeFilter().process_data(bframe.data, filter_chroms=["chr7"])
        print(len(bframe.data.keys()))
        print(bframe)
        self.assertEqual(len(bframe.data.keys()),2,"error chromosome filter")


