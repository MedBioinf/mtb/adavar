import unittest, os
import adagenes as ag


class TestPositionFilterTestCase(unittest.TestCase):

    def test_position_filter(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../test_files/tp53.interpreter.avf"
        bframe = ag.read_file(infile)

        print(bframe.data.keys())

        print(len(list(bframe.data.keys())))
        bframe = ag.filter_positions(bframe, 7676589, op="greater_than")

        print(len(list(bframe.data.keys())))

        self.assertEqual(len(list(bframe.data.keys())), 5, "")

