

import unittest, os
import adagenes


class TestRandomVariantFilter(unittest.TestCase):

    def test_random_variant_filter(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        genome_version = 'hg19'

        infile = __location__ + "/../test_files/TumorVariantDownload_r20.uta.avf"
        num=10
        bframe = adagenes.read_file(infile, genome_version=genome_version)

        self.assertEqual(len(list(bframe.data.keys())), 2107, "")
        print(len(bframe.data.keys()))
        bframe = adagenes.RandomVariantFilter().process_data(bframe.data,num)
        print(len(bframe.data.keys()))

        self.assertEqual(len(list(bframe.data.keys())), 10, "")


