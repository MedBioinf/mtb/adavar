import unittest, os
import adagenes as ag


class TestUpsetPlot(unittest.TestCase):

    def test_upset_plot(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))

        qid="chr7:140753336A>T"
        infile = __location__ + "/../../test_files/cf5221.clinsig.avf"
        bf = ag.read_file(infile)
        #variant_data = {qid:{}}
        variant_data = bf.data
        variant_data = ag.aggregate_evidence_data_by_pmid(variant_data, databases=None)
        ag.plot_evidence_database_intersections_upset(variant_data, show_plot=True)

    def test_upset_plot_drugs(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))

        patients = [
            "cf5221dd-14d4-4d80-847c-5de9963233b3.wxs.aliquot_ensemble_masked.maf.gz.uta.clinsig.avf",
            "cd0a18ea-acc3-4228-848e-2f8ca4c3257a.wxs.aliquot_ensemble_masked.maf.gz.uta.clinsig.avf",
            "462ff53d-8a58-4bc8-8a4b-c36f79f2c361.wxs.aliquot_ensemble_masked.maf.gz.uta.clinsig.avf"
        ]

        patient_data = {}
        for pid in patients:
            file = __location__ + "/../../test_files/" + pid
            bf = ag.read_file(file)
            variant_data = bf.data
            variant_data = ag.aggregate_evidence_data_by_pmid_drugs(variant_data, databases=None)
            patient_data[pid] = variant_data
        ag.plot_evidence_database_intersections_upset_drugs(patient_data, show_plot=True)

    def test_upset_plot_drug_classes(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))

        patients = [
            "cf5221dd-14d4-4d80-847c-5de9963233b3.wxs.aliquot_ensemble_masked.maf.gz.uta.clinsig.avf.uta.clinsig.drugclasses.avf",
            "cd0a18ea-acc3-4228-848e-2f8ca4c3257a.wxs.aliquot_ensemble_masked.maf.gz.uta.clinsig.avf.uta.clinsig.drugclasses.avf",
            "462ff53d-8a58-4bc8-8a4b-c36f79f2c361.wxs.aliquot_ensemble_masked.maf.gz.uta.clinsig.avf.uta.clinsig.drugclasses.avf"
        ]

        patient_data = {}
        for pid in patients:
            file = __location__ + "/../../test_files/" + pid
            bf = ag.read_file(file)
            variant_data = bf.data
            variant_data = ag.aggregate_evidence_data_by_pmid_drug_classes(variant_data, databases=None)
            patient_data[pid] = variant_data
        ag.plot_evidence_database_intersections_upset_drug_classes(patient_data, show_plot=True)

