
import adagenes

biomarker_data = {
    "ch1": { "revel": {"Score": "0.1"},
             "mvp": { "Score": "0.98" }
             },
    "ch2": { "revel": {"Score": "0.9"},
                 "mvp": { "Score": "0.2" }
                 },
    "ch3": { "revel": {"Score": "0.9"},
                 "mvp": { "Score": "0.9" }
                 },
    "ch4": { "revel": {"Score": "0.1"},
                 "mvp": { "Score": "0.1" }
                 },
    "ch5": { "revel": {"Score": "0.1"},
                 "mvp": { "Score": "0.3" }
                 },
    "ch6": { "revel": {"Score": "0.2"},
                 "mvp": { "Score": "0.9" }
                 }
}
modules = ["revel","mvp"]
keys=["Score", "Score"]

y_true = [2, 2, 2, 1, 1, 2]
#adagenes.plot.plot_roc_curves(biomarker_data, modules, keys, y=y_true)
#plot_roc_curves_def_vals(None)

