import unittest, os
import adagenes as ag


class TestPathogenicityHeatmapPlotGeneration(unittest.TestCase):

    def test_pathogenicity_heatmap_plot_generation(self):
        #variants = {"TP53:V143A":{},"TP53:R175H":{},"TP53:K132Q":{},"TP53:E285K":{},"TP53:R280K":{},"TP53:R273H":{}}
        #score_labels_raw = ["REVEL_rankscore", "CADD_raw_rankscore", "AlphaMissense_rankscore", "ESM1b_rankscore",
        #                    "EVE_rankscore"]
        #score_data = ag.generate_pathogenicity_plot_data(variants, score_labels_raw)
        #print(score_data)

        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../../test_files/tp53.interpreter.avf"
        bframe = ag.read_file(infile)
        # print(variant_data)
        score_labels_raw = ["REVEL_rankscore", "CADD_raw_rankscore", "AlphaMissense_rankscore", "ESM1b_rankscore",
                            "EVE_rankscore"]
        score_labels_display = ["REVEL", "CADD", "AlphaMissense", "ESM1b", "EVE"]

        data = ag.generate_pathogenicity_plot_data(bframe, score_labels_raw)
        print(data)


