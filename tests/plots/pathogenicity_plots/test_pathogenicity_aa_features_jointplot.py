import unittest, os
import adagenes as ag


class TestAAFeaturesHeatmapPlotGeneration(unittest.TestCase):

    def test_aafeatures_heatmap_plot_generation(self):
        #variants = {"TP53:V143A":{},"TP53:R175H":{},"TP53:K132Q":{},"TP53:E285K":{},"TP53:R280K":{},"TP53:R273H":{}}
        #score_labels_raw = ["REVEL_rankscore", "CADD_raw_rankscore", "AlphaMissense_rankscore", "ESM1b_rankscore",
        #                    "EVE_rankscore"]
        #score_data = ag.generate_aa_features_plot_data(variants)

        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../../test_files/tp53.interpreter.avf"
        bframe = ag.read_file(infile)
        #print(variant_data)
        score_labels_raw = ["REVEL_rankscore", "CADD_raw_rankscore", "AlphaMissense_rankscore", "ESM1b_rankscore",
                            "EVE_rankscore"]
        score_labels_display = ["REVEL", "CADD", "AlphaMissense", "ESM1b", "EVE"]

        #bframe.data = ag.SectionFilter().process_data(bframe.data, "UTA_Adapter")
        #bframe.data = ag.SectionFilter().process_data(bframe.data, "dbnsfp")

        #vars = []
        #for var in bframe.data.keys():
        #    add_var = True
        #    for score in score_labels_raw:
        #        if (bframe.data[var]["dbnsfp"][score] == "") or (bframe.data[var]["dbnsfp"][score] == "."):
        #            add_var = False
        #            print("null value found")
        #    if add_var is True:
        #        vars.append(var)
        #bframe.data = ag.DefinedVariantFilter().process_data(bframe.data, vars)

        bframe = ag.positional_sort(bframe)
        bframe = ag.filter_same_position(bframe)
        bframe = ag.positional_sort(bframe)
        print("Num variants: ",len(list(bframe.data.keys())))

        variant_labels, score_data, zdata = ag.generate_pathogenicity_aa_features_joint_data(bframe, score_labels_raw)

        print("score data ",len(score_data), ":", len(score_data[0]), ":", len(score_data[1]),":", len(score_data[2]),
              ":", len(score_data[3]), ":", len(score_data[4]))
        print("zdata ",zdata)
        print("variants ",len(variant_labels))

        #score_data = ag.generate_pathogenicity_plot_data(bframe, score_labels_raw)
        fig = ag.generate_protein_pathogenicity_plot(score_data, x_title="Score", y_title="TP53",
                                                     x_labels=variant_labels, y_labels=score_labels_display)
        fig.show()

        fig = ag.generate_aa_features_heatmap(None, zdata=zdata, variant_labels=variant_labels, showlegend=False)
        fig.show()

