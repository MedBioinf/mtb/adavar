import unittest, os
import adagenes as av


class TestSunburstTestCase(unittest.TestCase):

    def test_sunburst_plots_all_biomarkers_drug_class(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../../test_files/chr7_140753336A_T.annotated.avf"
        bframe = av.read_file(infile)

        plot_type = "treatments-all-sunburst-match-type-drug-classes-all"
        fig = av.generate_sunburst_plot(bframe.data, "", plot_type, response_type="fig")
        fig.show()

    def test_sunburst_plots_all_biomarkers_drugs(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../../test_files/chr7_140753336A_T.annotated.avf"
        bframe = av.read_file(infile)

        plot_type = "treatment-sunburst-match-type-drugs"
        fig = av.generate_sunburst_plot(bframe.data, "chr7:140753336A>T", plot_type, response_type="fig")
        fig.show()

