import unittest, os
import adagenes


class TestDatasetCombination(unittest.TestCase):

    def test_dataset_combination(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        file1 = __location__ + "/../test_files/somaticMutations.ln50.txt"
        file2 = __location__ + "/../test_files/somaticMutations.ln50.2.txt"
        genome_version="hg19"
        mapping = {
            "chrom": 1,
            "pos": 2,
            "ref": 4,
            "alt": 5
        }
        bframe = adagenes.TXTReader().read_file(file1, genome_version=genome_version, mapping=mapping, sep="\t")
        print(bframe.get_ids())
        bframe1 = adagenes.TXTReader().read_file(file2, genome_version=genome_version, mapping=mapping, sep="\t")
        print(bframe1.get_ids())
        bframe = adagenes.merge_bframes(bframe, bframe1)
        print(len(bframe.data.keys()))
        self.assertEqual(len(bframe.data.keys()),96, "Error: Expected length of combined biomarkers do not match")


