import unittest
import adagenes as ag


class ProteinToGenomicTestCase(unittest.TestCase):

    def test_refseq_protein_to_gene(self):
        bframe = ag.BiomarkerFrame("NP_000537.3")
        bframe = ag.refseq_protein_to_gene(bframe)
        print(bframe.data)
        self.assertEqual(list(bframe.data.keys()), ["TP53"], "")
        self.assertEqual(bframe.data["TP53"]["mane"]["ensembl_id"], "ENSP00000269305.4", "")

