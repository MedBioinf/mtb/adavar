import unittest, os
import adagenes


class TestDatasetSplit(unittest.TestCase):

    def test_dataset_split(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        file1 = __location__ + "/../test_files/somaticMutations.ln50.txt"
        genome_version="hg19"
        mapping = {
            "chrom": 1,
            "pos": 2,
            "ref": 4,
            "alt": 5
        }
        bframe = adagenes.TXTReader().read_file(file1, genome_version=genome_version, mapping=mapping, sep="\t")
        print(bframe.get_ids())

        bframe1,bframe2 = adagenes.split_bframe(bframe, size_set1="0.2p", size_set2="0.8p")

        print(len(bframe.data.keys()))
        print(len(bframe1.data.keys()))
        print(len(bframe2.data.keys()))

        print(bframe1.data)
        print(bframe2.data)

        self.assertEqual(len(bframe1.data.keys()),9, "Error: Expected length of bframe does not match")
        self.assertEqual(len(bframe2.data.keys()), 38, "Error: Expected length of bframe does not match")


