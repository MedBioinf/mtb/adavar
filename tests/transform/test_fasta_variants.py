import unittest
import adagenes as ag

class FastaVariantsTestCase(unittest.TestCase):

    def test_fasta_variant_bframe(self):
        bframe = {' EMP1; NM_001423.3': {'sequence': 'MLVLLAGIFVVHIATVIMLFVSTIANVWLVSNTVDASVGLWKNCTNISCSDSLSYASEDALKTVQAFMILSIIFCVIALLVFVFQLFTMEKGNRFFLSGATTLVCWLCILVGVSIYTSHYANRDGTQYHHGYSYILGWICFCFSFIIGVLYLVLRKK', 'mutation_type': 'unidentified', 'mdesc': 'unidentified', 'orig_identifier': ' EMP1; NM_001423.3'}}
        bframe = ag.generate_variant_fasta_in_bframe(bframe,' EMP1; NM_001423.3', pos=0, ref_aa="M", alt_aa="A")
        print(bframe)
        self.assertEqual(bframe[' EMP1; NM_001423.3']["sequence"][0],"A","")
