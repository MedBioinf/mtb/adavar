import unittest, os
from adagenes.tools.client_mgt import get_reader, get_writer
from adagenes.clients import XLSXReader, VCFWriter


class TestXLSXReaderWriter(unittest.TestCase):

    def test_xlsx_reader(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))

        input_file = "../../test_files/somaticMutations.ln_4.xlsx"
        genome_version="hg38"
        mapping = {
            "chromosome": 0,
            "position": 1,
            "ref": 2,
            "alt": 3
        }

        reader = XLSXReader()
        #json_obj = reader.read_file(input_file,genome_version=genome_version, mapping=mapping)
        #print(json_obj.data)




