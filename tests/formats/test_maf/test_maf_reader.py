import unittest, os
import adagenes


class TestMAFReader(unittest.TestCase):

    def test_maf_reader(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../../test_files/somaticMutations.tcga.brca.maf"
        genome_version = "hg38"

        #bframe = adagenes.MAFReader().read_file(infile,genome_version=genome_version, max_length=100)
        bframe = adagenes.read_file(infile, genome_version=genome_version)
        # filter samples
        #data.data = adagenes.FeatureFilter().process_data(data.data, module="variant_data", feature="Tumor_Sample_Barcode", val="TCGA-02-0003-01A-01D-1490-08")

        #print(bframe.data)
        #print(data.data.keys())
        self.assertEqual(len(bframe.data.keys()),14,"Could not read variants")


